import argparse
import matplotlib.pyplot as plt

import chainer
from chainer import training,serializers
from chainercv.datasets import voc_bbox_label_names
from chainercv.links import FasterRCNNVGG16
from chainercv import utils
from chainercv.visualizations import vis_bbox


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', type=int, default=-1)
    parser.add_argument('--pretrained-model', default='voc07')
    parser.add_argument('image')
    parser.add_argument('--mini', '-m', type=int, default=0)
    parser.add_argument('--epoch', '-e', type=int, default=100)
    args = parser.parse_args()

    c_label = 4
    folder = 'result/normal/epoch_{}'.format(args.epoch)
    label_names = ['umaru', 'ebina', 'kirie', 'silfin']
    if args.mini:
        print('mini')
        c_label = 1
        folder = 'result/mini/epoch_{}'.format(args.epoch)
        label_names = ['umaru']
    else:
        print('normal')
        c_label = 4
        folder = 'result/normal/epoch_{}'.format(args.epoch)
        label_names = ['umaru', 'ebina', 'kirie', 'silfin']


    model = FasterRCNNVGG16(n_fg_class=c_label, pretrained_model=folder)
    if args.gpu >= 0:
        chainer.cuda.get_device_from_id(args.gpu).use()
        model.to_gpu()
    img = utils.read_image(args.image, color=True)
    bboxes, labels, scores = model.predict([img])
    bbox, label, score = bboxes[0], labels[0], scores[0]
    print(bbox, label, score)
    vis_bbox(
        img, bbox, label, score, label_names=label_names)
    plt.show()


if __name__ == '__main__':
    main()
