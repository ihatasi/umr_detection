## 必要なもの
----
私の環境はPython3.6, Chainer5.0.0, chainercv0.11.0でしたが，多少バージョンが違っても動きます．ただし，chainercv0.8.0以下では"from chainercv.chainer_experimental.datasets.sliceable import TupleDataset"でエラーが出ました．
pred.pyだけなら関係ないので大丈夫だとは思います． <br>[記事](https://ihatasi.hatenablog.com/entry/2018/12/19/000003)にも書いてある通り学習済みモデルを[ここ](https://drive.google.com/drive/folders/1w9DlIJ-Upt9boZJL__0wrv_IjZ1Hrx90?usp=sharing)からダウンロードしてください(フォルダ名は違うが，ファイル名が同じなので注意)．いくつかepochから始まるファイルがあると思いますが全部ダウンロードする必要はなく，必要な分だけダウンロードしてください（迷ったらepoch_100だけでいいと思います）．   <br>リンク先にはminiとnormalのフォルダがそれぞれあると思いますが，それはこのプロジェクト内のresult/miniとresult/normalに対応しています．
result/mini/epoch_*, result/normal/epoch_*のようなファイル構造になるように入れてください（pythonを触ったことがある人は自分の好きなところに置いて好きなようにプログラム内で指定してください．）．
## 実行例
`python pred.py hoge.jpg -m 1 -e 100` <br>`hoge.jpg`は自分で用意する画像． `-m`は1ならミニうまるちゃん，0ならうまるず．`-e`は学習モデルの数字に対応．

## for Mac
Macで動くことを確認しました(python3)．<br>
`pip3 install chainer`(2018/12/14時点ではchainer5.1.0が入る)<br>
`pip3 install chainercv`(2018/12/14時点ではchainercv0.11.0が入る)<br>
の2つを実行して実行例のようにコマンドラインに入力すると動きます．