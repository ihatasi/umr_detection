import xml.etree.ElementTree as ET
from PIL import Image
import numpy as np
import os
from chainercv.chainer_experimental.datasets.sliceable import TupleDataset
from chainercv.utils import generate_random_bbox
"""
xml.etree.ElementTreeを感覚で使っているのでfindのところが雑になってる．
"""

def make_dataset(mini=False):
    imgs = []
    labels = []
    bboxes = []
    if mini:
        path = 'mini/'
    else:
        paht = 'normal/'

    #miniフォルダにあるファイル名をすべて取得
    fname =os.listdir(path)
    #jpgとxmlが交互に入っているので1つおきに取得
    jf = fname[::2]
    xf = fname[1::2]
    for a,b in zip(jf, xf):
        #画像の処理
        jpath = os.path.join(path, a)
        img = Image.open(jpath)
        img = np.asarray(img, dtype=np.float32)
        #Chainer用の画像ファイルの形に
        img = img.transpose(2, 0, 1)
        #bboxとlabelの処理
        xpath = os.path.join(path, b)
        data = ET.parse(xpath)
        root = data.getroot()
        tmp_bboxes = np.array([[0,0,0,0]]).astype(np.float32)
        tmp_labels = np.array([[]]).astype(np.int32)
        for element in root.findall(".object"):
            label = element.findtext('.//name')
            ymin = element.findtext('.//ymin')
            xmin = element.findtext('.//xmin')
            ymax = element.findtext('.//ymax')
            xmax = element.findtext('.//xmax')
            bbox = np.array([[ymin, xmin, ymax, xmax]], dtype=np.float32)
            label = np.array([label], dtype=np.int32)
            tmp_bboxes = np.append(tmp_bboxes, bbox, axis=0)
            tmp_labels = np.append(tmp_labels, label)
        #add
        imgs.append(img)
        labels.append(tmp_labels)
        bboxes.append(tmp_bboxes[1:])

    dataset = TupleDataset(('img', imgs), ('bbox', bboxes), ('label', labels))
    return dataset